use std::f64::consts::PI;

enum Shape<T> {
    Circle(T),
    RightTriangle(T, T),
    Rectangle(T, T),
}

impl<T: Copy + std::convert::From<f64> + Into<f64>> Shape<T> {
    
    fn area(self) -> f64 {
        match self {
            Shape::Circle(diameter) => (diameter.into() / 2.0) * (diameter.into() / 2.0) * PI,
            Shape::RightTriangle(base, height) => (base.into()) * (height.into()) / 2.0,
            Shape::Rectangle(width, length) => (width.into()) * (length.into()),
        }
    }
}

fn main() {
    
    let base = 24_u8;
    let height = 24_u8;
    let triangle = Shape::RightTriangle(base as f64, height as f64);
    let triangle_area = triangle.area();
    let width = 12_u8;
    let length = 24_u8;
    let rectangle = Shape::Rectangle(width as f64, length as f64);
    let rectangular_area = rectangle.area();
    let diameter = 45_u8;
    let circle = Shape::Circle(diameter as f64);
    let circle_area = circle.area();

    // This part below is fixed. Ignore.
    println!(
        "The area of the triangle with a base of {} and a height of {} is {:.5}",
        base, height, triangle_area
    );

    println!(
        "The area of the rectangle with a width of {} and a length of {} is {:.5}",
        width, length, rectangular_area
    );

    println!(
        "The area of the circle with a diameter of {} is {:.5}",
        diameter, circle_area
    );
}
